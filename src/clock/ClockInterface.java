package clock;

/**
 * An interface for a 24 hour clock (military time).
 *
 */
public interface ClockInterface {

	/*
	 * The typical getters that you would expect.
	 */
	public abstract int getSeconds();

	public abstract int getMinutes();

	public abstract int getHours();

	/*
	 * set seconds to sec % 60
	 */
	public abstract void setSeconds(int sec);
	/*
	 * set minutes to min % 60
	 */
	public abstract void setMinutes(int min);

	/*
	 * set hours to hr % 24
	 */
	public abstract void setHours(int hr);

	/**
	 * Set the time to the specified time.
	 * 
	 * @param second desired seconds
	 * @param minute desired minutes
	 * @param hour desired hours
	 */
	public abstract void setTime(int hr, int min, int sec);

	/**
	 * Add one to seconds. If seconds goes to 60, sets it to 0 and increments minutes.
	 */
	public abstract void incrementSeconds();

	/**
	 * Add one to minutes. If minutes goes to 60, sets it to 0 and increments hours.
	 */
	public abstract void incrementMinutes();

	/**
	 * Add 1 the hour. If hour goes to 13, it sets it to 1.
	 */
	public abstract void incrementHours();

}